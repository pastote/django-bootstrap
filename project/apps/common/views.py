from django.shortcuts import render
import random

def home(request):
    """View function for home page of site."""

    return render(request, 'home.html', {'numero': random.randint(100, 2300)})
